import { products } from "./data.js"

let counter = document.querySelector('.counter_favorite')
let counter2 = document.querySelector('.counter_card')

const MANAGE_CART_ITEM_COUNT = (bool, id) => {
    let find = products.filter(item => item.id == id)[0]
    let newLocal = JSON.parse(localStorage.cart)

    if (bool) {
        find.counter++

        newLocal.push(id)
    } else {
        find.counter--

        if (newLocal.includes(id)) {
            let idx = newLocal.indexOf(id)
            newLocal.splice(idx, 1)
        } else {
            DELETE_CART_ITEM(id)
        }
    }

    localStorage.cart = JSON.stringify(newLocal)
}

if (localStorage.cart) {
    let cartId = JSON.parse(localStorage.cart) || []
    // let cartObj = []
    let cart = []
    let table = document.querySelector("table tbody")


    cart = products.filter(item => JSON.parse(localStorage.cart).includes(item.id))


    for (let item of cart) {
        item.counter = 0

        for (let local of JSON.parse(localStorage.cart)) {
            if (local == item.id) {
                item.counter++
            }
        }
    }

    for (let item of products) {
        for (let id of cartId) {
            if (item.id == id) {
                if (!cart.includes(item)) {
                    cart.push(item)
                }
            }
        }
    }

    const CREATE_TABLE = () => {
        let price = []
        let counts = []
        for (let item of cart) {
            price.push(Math.round(+item.price - (+item.price / 100 * +item.sale)))
            counts.push(item.counter)
            let tr = document.createElement("tr")
            let td_idx = document.createElement("td")
            let td_name = document.createElement("td")
            let td_count = document.createElement("td")

            let min = document.createElement('span')
            let plus = document.createElement('span')
            let remove = document.createElement('td')
            let button = document.createElement('button')

            let td_sale = document.createElement("td")
            let td_dis = document.createElement('td')
            let end_price = document.createElement('td')
            let num = document.createElement('td')
            let move = document.createElement('div')
            let coun = document.createElement('p')
            let disc = 10
            table.classList.add('tab')
            move.classList.add('move')
            coun.classList.add('coun')

            remove.setAttribute('char', '%')
            remove.setAttribute('align', 'char')

            min.onclick = () => {
                window.location.reload()
                MANAGE_CART_ITEM_COUNT(false, item.id)
            }

            plus.onclick = () => {
                window.location.reload()
                MANAGE_CART_ITEM_COUNT(true, item.id)
            }


            min.innerText = '-'
            plus.innerText = '+'
            min.style.cursor = 'pointer'
            plus.style.cursor = "pointer"
            min.style.border = '1px solid silver'
            plus.style.border = '1px solid silver'
            min.style.margin = '0 5px'
            plus.style.margin = '0 5px'
            coun.innerHTML = +(JSON.parse(localStorage.cart).filter(id => id === item.id)).length
            td_idx.innerText = +cart.indexOf(item) + 1
            td_name.innerText = item.name
            td_count.innerText = +(JSON.parse(localStorage.cart).filter(id => id === item.id)).length
            td_sale.innerText = `${item.price} $`
            td_dis.innerText = +Math.floor(+item.price - (+item.price * disc) / 100) + '$'
            button.innerText = 'Remove All'
            button.classList.add('learn-more')


            remove.append(button)
            td_count.prepend(min)
            td_count.append(plus)
            tr.append(td_idx, td_name, td_count, td_sale, td_dis, end_price, num, move, remove)
            table.append(tr)


            button.onclick = () => {
                window.location.reload()
                DELETE_CART_ITEM(item.id)
            }

        }
        let tr = document.createElement('tr')
        let empty1 = document.createElement('td')
        let empty2 = document.createElement('td')
        let empty3 = document.createElement('td')
        let allprice = document.createElement('td')
        let col = document.createElement('td')
        let empty = document.createElement('td')
        if (price.length) {
            let sum = price.reduce((a, b) => a + b)
            allprice.innerText = 'Итог: ' + sum + '$'
        }
        if (cart.length) {
            col.innerText = 'Итогo: ' + counts.reduce((a, b) => a + b) + ' товаров'
        }

        tr.append(empty1, empty2, empty3, allprice, col, empty)
        table.append(tr)

        if (localStorage.like && localStorage.like.length) {
            counter.innerText = products.filter(item => JSON.parse(localStorage.like).includes(item.id)).length
        }
        if (localStorage.cart && localStorage.cart.length) {
            counter2.innerText = products.filter(item => JSON.parse(localStorage.cart).includes(item.id)).length
        }

    }

    CREATE_TABLE()

    const DELETE_CART_ITEM = (id) => {
        cart = cart.filter(item => item.id !== id)

        localStorage.cart = JSON.stringify(cart.map(item => item.id))
    }

}