import { products  } from "./data.js"
import { ADD_TO_CART, REMOVE_TO_CART, ADD_TO_LIKE, REMOVE_TO_LIKE } from "./likes_hearts.js"
let random_text = document.querySelectorAll('.mackbook')
let image_ran = document.querySelectorAll('.shit_img')
let link_a = document.querySelectorAll('.a_link')

// const random_1 = (arr, num) => {
//   let random = +(Math.random() * (arr.length - 0) + 0).toString().slice(0, 2)
//   image_ran[num].src = arr[random].images[0]
//   random_text[num].innerHTML = arr[random].name
//   link_a[num].href = './product-info.html#' + arr[random].id
// }
// random_1(products.filter(item => item.category === 4), 0)
// random_1(products.filter(item => item.category == 2), 1)
// random_1(products.filter(item => item.category === 2), 2)


const RENDER_PRODUCTS = (arr, id, limit) => {
  let place = document.querySelector(`#${id}`)

  let tempArr = arr.filter(() => true)
  tempArr.splice(0, limit)

  let btnShow = document.createElement('button')
  let btnHide = document.createElement('button')
  btnShow.innerHTML = 'Show all'
  btnHide.innerHTML = 'Hide all'
  btnShow.classList.add('btn__all', 'show__all')
  btnHide.classList.add('btn__all')
  btnHide.classList.add('hide__all')

  btnShow.onclick = () => {
    RENDER_PRODUCTS(arr, id, arr.length)
    btnHide.classList.add('hide__all')
    place.append(btnHide)
  }
  btnHide.onclick = () => {
    btnHide.style.display = 'none'
    RENDER_PRODUCTS(arr, id, 5)
  }

  place.innerHTML = ''
  place.append(btnShow)
  for (let item of arr) {
    let div = document.createElement('a')
    let btnLike = document.createElement('button')
    let likeIcon = document.createElement('img')
    let likeGreen = document.createElement('img')
    let img = document.createElement('img')
    let category = document.createElement('p')
    let title = document.createElement('p')
    let btnColors = document.createElement('div')
    let grey = document.createElement('button')
    let light = document.createElement('button')
    let dark = document.createElement('button')
    let gold = document.createElement('button')
    let product_desc = document.createElement('div')
    let product_prise = document.createElement('p')
    let like_comm = document.createElement('div')
    let likes = document.createElement('p')
    let like = document.createElement('span')
    let imgLike = document.createElement('img')
    let comments = document.createElement('p')
    let comment = document.createElement('span')
    let imgComment = document.createElement('img')
    let button = document.createElement('button')
    let img_heart = document.createElement('img')

    category.innerHTML = item.category
    title.innerHTML = item.name
    product_prise.innerHTML = `${item.price} <br> $`
    like.innerHTML = '4'
    comment.innerHTML = '25'
    div.href = `./product-info.html#${item.id}`
    button.innerHTML = 'Добавить'

    // CART
    if (localStorage.cart) {
      let cart = JSON.parse(localStorage.cart)

      if (cart.includes(item.id)) {
        button.classList.add('active-btn')
        button.innerHTML = 'Добавлено'
      }
    }
    button.onclick = () => {
      ADD_TO_CART(item.id)
      button.classList.add('active-btn')
      button.innerHTML = 'Добавлено'
    }


    // LIKE
    let counter_like = document.querySelector('.counter_favorite')
    if (localStorage.like) {
      let like = JSON.parse(localStorage.like)

      if (like.includes(item.id)) {
        likeIcon.style.display = 'none'
        likeGreen.style.display = 'block'
      } else {
        likeIcon.style.display = 'block'
        likeGreen.style.display = 'none'
        img_heart.src = 'http://static.skaip.su/img/emoticons/180x180/f6fcff/heartgreen.gif'

      }
    } else {
      likeGreen.style.display = 'none'
    }

    likeIcon.onclick = () => {
      ADD_TO_LIKE(item.id)
      likeIcon.style.display = 'none'
      likeGreen.style.display = 'block'
    }

    likeGreen.onclick = () => {
      REMOVE_TO_LIKE(item.id)
      likeIcon.style.display = 'block'
      likeGreen.style.display = 'none'
    }


    div.classList.add('active')
    div.classList.add('product')
    btnLike.classList.add('like_green')
    img.classList.add('product_img')
    category.classList.add('categories')
    title.classList.add('product_name')
    btnColors.classList.add('btns')
    grey.classList.add('color', 'grey')
    light.classList.add('color', 'light')
    dark.classList.add('color', 'dark')
    gold.classList.add('color', 'gold')
    product_desc.classList.add('product_desc')
    product_prise.classList.add('product_prise')
    like_comm.classList.add('like_comm')
    likes.classList.add('likes', 'l')
    like.classList.add('like')
    comments.classList.add('comments', 'l')
    comment.classList.add('comment')
    button.classList.add('buy')
    likeIcon.classList.add('like-icon')
    likeGreen.classList.add('like-green')

    img.setAttribute('src', `${item.images[0]}`)
    likeIcon.setAttribute('src', './images/icons/green-like.png')
    likeGreen.setAttribute('src', './img/hearts.png')
    imgLike.setAttribute('src', './images/icons/btn-star.svg')
    imgComment.setAttribute('src', './images/icons/btn-commend.svg')

    likes.append(like, imgLike)
    comments.append(imgComment, comment)
    like_comm.append(likes, comments)
    product_desc.append(product_prise, like_comm)
    btnColors.append(grey, light, dark, gold)
    btnLike.append(likeIcon, likeGreen)
    div.append(btnLike, img, category, title, btnColors, product_desc, button)
    place.append(div)

    if (arr.indexOf(item) >= limit) {
      div.style.display = 'none'
      div.classList.remove('active')
    }
  }
}

let sorted = products.sort((a, b) => b.price - a.price);
let cheap = []
let tvs = []
let phones = []
let access = []
let laptops = []

for (let item of products) {
  item.category == 1 ? tvs.push(item) : console.log();
  item.category == 2 ? phones.push(item) : console.log();
  item.category == 3 ? access.push(item) : console.log();
  item.category == 4 ? laptops.push(item) : console.log();
  if (item.price < 2000) {
    cheap.push(item)
    cheap.sort((a, b) => b.price - a.price)
    cheap.reverse()
  }
}

if (document.body.getAttribute('id') == 'home') {
  RENDER_PRODUCTS(sorted, 'place-for-expensive', 5)
  RENDER_PRODUCTS(cheap, 'place-for-cheaper', 5)
  RENDER_PRODUCTS(phones, 'place-for-phones', 5)
  RENDER_PRODUCTS(access, 'place-for-access', 5)
}
if (document.body.getAttribute('id') == 'catalog-products') {
  RENDER_PRODUCTS(tvs, 'place-for-tv', 5)
  RENDER_PRODUCTS(phones, 'place-for-phones', 5)
  RENDER_PRODUCTS(laptops, 'place-for-laptop', 5)
  RENDER_PRODUCTS(access, 'place-for-accessory', 5)
}

if (document.body.getAttribute('id') == 'pagination') {
  RENDER_PRODUCTS(products, 'place-for-all', products.length)
}

if (document.body.getAttribute('id') == 'product-info') {
  for (let item of products) {
    if (item.category == 1) {
      RENDER_PRODUCTS(tvs, 'place-for-rec', 5)
    }
    if (item.category == 2) {
      RENDER_PRODUCTS(phones, 'place-for-rec', 5)
    }
    if (item.category == 3) {
      RENDER_PRODUCTS(access, 'place-for-rec', 5)
    }
    if (item.category == 4) {
      RENDER_PRODUCTS(laptops, 'place-for-rec', 5)
    }
  }
}

export { RENDER_PRODUCTS, tvs, phones, access, laptops }