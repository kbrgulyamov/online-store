let btnCart = document.querySelector('.buy')
let btnLike = document.querySelector('.like_green')

let cart = []
let like = []

// CART
if (localStorage.cart) {
   cart = JSON.parse(localStorage.cart)
}

const ADD_TO_CART = (id) => {
   event.preventDefault()
   cart.push(id)

   localStorage.cart = JSON.stringify(cart)
   let counter_card = document.querySelector('.counter_card')
   if (cart.length) {
      counter_card.innerHTML = cart.length
   }
}

const REMOVE_TO_CART = (id) => {
   event.preventDefault()
   cart.pop(id)

   localStorage.cart = JSON.stringify(cart)
   let counter_card = document.querySelector('.counter_card')
   if (cart.length) {
      counter_card.innerHTML = cart.length
   }
}

// LIKE
if (localStorage.like) {
   like = JSON.parse(localStorage.like)
}
const ADD_TO_LIKE = (id) => {
   event.preventDefault()
   like.push(id)

   localStorage.like = JSON.stringify(like)
   let counter_like = document.querySelector('.counter_favorite')
   if (like.length) {
      counter_like.innerHTML = like.length
   }
}

const REMOVE_TO_LIKE = (id) => {
   event.preventDefault()
   like.pop(id)

   localStorage.like = JSON.stringify(like)
   let counter_like = document.querySelector('.counter_favorite')

   if (like.length) {
      counter_like.innerHTML = like.length
   }
}

export { ADD_TO_CART, REMOVE_TO_CART, ADD_TO_LIKE, REMOVE_TO_LIKE }
